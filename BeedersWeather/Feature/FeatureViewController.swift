//
//  FeatureViewController.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 06/07/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

class FeatureViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        <#code#>
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        <#code#>
    }

}
