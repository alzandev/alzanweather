//
//  ActivityIndicator.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 29/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

class ActivityIndicator {
    static func showActivityIndicator(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Title", message: "Loading...", preferredStyle: .alert)
        
        let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)
        
        progressDownload.setProgress(5.0/10.0, animated: true)
        progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        
        alertController.view.addSubview(progressDownload)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
