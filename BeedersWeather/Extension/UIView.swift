//
//  UIView.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 28/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

extension UIView {
    func setGradientBackground(main: UIColor, second: UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [main.cgColor, second.cgColor]
        gradientLayer.locations = [0, 1]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func shadowLayout(){
        self.layer.cornerRadius = 8.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 5.0
        self.layer.shadowOpacity = 0.4
    }
}
