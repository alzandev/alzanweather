//
//  String.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 28/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

extension String {
    func stringToDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.date(from: self)
        
        return date!
    }
    
    func stringToCustomDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        
        let date = dateFormatter.date(from: self)
        
        return date!
    }
}
