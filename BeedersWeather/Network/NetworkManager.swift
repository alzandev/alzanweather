//
//  NetworkManager.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 24/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import Alamofire

protocol NetworkManager {
    typealias Method = Alamofire.HTTPMethod
    typealias Handler = (DataResponse<Any>) -> Void
    
    static func requestJSON(url: String, method: Method, completion: @escaping Handler)
}

extension NetworkManager {
    static func requestJSON(url: String, method: Method, completion: @escaping Handler){
        Alamofire.request(url, method: method)
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: completion)
    }
}
