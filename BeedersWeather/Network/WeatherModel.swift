//
//  WeatherModel.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 24/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

typealias JSONDict = [String : Any]

protocol WeatherModel {
    var consolidatedWeather: [ConsolidatedWeather] { get }
    var title: String { get }
    var locationType: String { get }
    var woeid: Int { get }
    var lattLong: String { get }
    var timezone: String { get }
    var sunrise: String { get }
    var sunset: String { get }
}

struct WeatherModelFields: WeatherModel {
    var consolidatedWeather: [ConsolidatedWeather]
    var title: String
    var locationType: String
    var woeid: Int
    var lattLong: String
    var timezone: String
    var sunset: String
    var sunrise: String
    
    init(jsonDict: JSONDict){
        title = jsonDict["title"] as! String
        locationType = jsonDict["location_type"] as! String
        woeid = jsonDict["woeid"] as! Int
        lattLong = jsonDict["latt_long"] as! String
        timezone = jsonDict["timezone"] as! String
        sunrise = jsonDict["sun_rise"] as! String
        sunset = jsonDict["sun_set"] as! String

        
        if let consolidatedWeatherDict = jsonDict["consolidated_weather"] as? [JSONDict] {
            var consolidatedWeatherDictArray: [ConsolidatedWeather] = []
            for weatherDict in consolidatedWeatherDict {
                consolidatedWeatherDictArray.append(ConsolidatedWeatherFields(jsonDict: weatherDict))
            }
            consolidatedWeather = consolidatedWeatherDictArray
        } else {
            consolidatedWeather = []
        }
    }
    
    init(){
        title = ""
        locationType = ""
        woeid = 0
        lattLong = ""
        timezone = ""
        consolidatedWeather = []
        sunset = ""
        sunrise = ""
    }
}

protocol ConsolidatedWeather {
    var id: Int { get }
    var weatherStateName: String { get }
    var weatherStateAbbr: String { get }
    var windDirectionCompass: String { get }
    var created: String { get }
    var applicableDate: String { get }
    var minTemp: Double { get }
    var maxTemp: Double { get }
    var theTemp: Double { get }
    var windSpeed: Double { get }
    var windDirection: Double { get }
    var airPressure: Double { get }
    var humidity: Int { get }
    var visibility: Double { get }
    var predictability: Int { get }
}

struct ConsolidatedWeatherFields: ConsolidatedWeather {
    var id: Int
    var weatherStateName: String
    var weatherStateAbbr: String
    var windDirectionCompass: String
    var created: String
    var applicableDate: String
    var minTemp: Double
    var maxTemp: Double
    var theTemp: Double
    var windSpeed: Double
    var windDirection: Double
    var airPressure: Double
    var humidity: Int
    var visibility: Double
    var predictability: Int
    
    init(){
        id = 0
        weatherStateName = ""
        weatherStateAbbr = ""
        windDirectionCompass = ""
        created = ""
        applicableDate = ""
        minTemp = 0.0
        maxTemp = 0.0
        theTemp = 0.0
        windSpeed = 0.0
        windDirection = 0.0
        airPressure = 0.0
        humidity = 0
        visibility = 0.0
        predictability = 0
    }
    
    init (jsonDict: JSONDict){
        id = jsonDict["id"] as! Int
        weatherStateName = jsonDict["weather_state_name"] as! String
        weatherStateAbbr = jsonDict["weather_state_abbr"] as! String
        windDirectionCompass = jsonDict["wind_direction_compass"] as! String
        created = jsonDict["created"] as! String
        applicableDate = jsonDict["applicable_date"] as! String
        minTemp = jsonDict["min_temp"] as! Double
        maxTemp = jsonDict["max_temp"] as! Double
        theTemp = jsonDict["the_temp"] as! Double
        windSpeed = jsonDict["wind_speed"] as! Double
        windDirection = jsonDict["wind_direction"] as! Double
        airPressure = jsonDict["air_pressure"] as! Double
        humidity = jsonDict["humidity"] as! Int
        visibility = jsonDict["visibility"] as! Double
        predictability = jsonDict["predictability"] as! Int
    }
}
