//
//  Routes.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 24/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import Foundation

class Routes: NetworkManager {
    enum RouteHandler {
        typealias Success = (Data) -> Void
        typealias Error = (NSError) -> Void
    }
        
    static func routeWeather(completionHandler: @escaping(NSError?, WeatherModel) -> Void) {
        requestJSON(url: "https://www.metaweather.com/api/location/455827/", method: .get) { (response) in
            if let error = response.error {
                completionHandler(error as NSError, WeatherModelFields())
                return
            }
            
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments)
                
                if let error = response.error {
                    completionHandler(error as NSError, WeatherModelFields())
                }
                
                if let jsonResult = jsonResult as? JSONDict {
                    let weather = WeatherModelFields(jsonDict: jsonResult)
                    completionHandler(nil, weather)
                } else {
                    print("error")
                }
            } catch {
                print(error)
            }
        }
    }
}
