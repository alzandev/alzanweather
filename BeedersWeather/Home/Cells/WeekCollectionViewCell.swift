//
//  WeekCollectionViewCell.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 28/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

class WeekCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func initContent(consWeather: ConsolidatedWeather){
        dayLabel.text = consWeather.applicableDate.stringToDate().nameOfTheDay()
        statusImageView.image = UIImage(named: consWeather.weatherStateAbbr)
        tempMaxLabel.text = "\(Int(consWeather.maxTemp))º"
        tempMinLabel.text = "\(Int(consWeather.minTemp))º"
    }
}
