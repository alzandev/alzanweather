//
//  MoreDetailsCollectionViewCell.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 28/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit

class MoreDetailsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var sunriseView: UIView!
    @IBOutlet weak var sunsetView: UIView!
    @IBOutlet weak var airPressureView: UIView!
    @IBOutlet weak var windView: UIView!
    @IBOutlet weak var humidityView: UIView!
    @IBOutlet weak var visibilityView: UIView!
    
    
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var airPressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configLayout()
    }
    
    func configLayout(){
        sunriseView.shadowLayout()
        sunsetView.shadowLayout()
        airPressureView.shadowLayout()
        windView.shadowLayout()
        humidityView.shadowLayout()
        visibilityView.shadowLayout()
    }
    
    func initContent(weather: WeatherModel){
        sunriseLabel.text = weather.sunrise.stringToCustomDate().getTime()
        sunsetLabel.text = weather.sunset.stringToCustomDate().getTime()
        
        let windDirectionCompass = weather.consolidatedWeather.first?.windDirectionCompass
        let windSpeed = Int(weather.consolidatedWeather.first!.windSpeed)
        windLabel.text = "\(windDirectionCompass!) \(windSpeed)km/h"
        
        
        let airPressure = Int(weather.consolidatedWeather.first!.airPressure)
        airPressureLabel.text = "\(airPressure) hPa"
        
        let humidity = weather.consolidatedWeather.first!.humidity
        humidityLabel.text = "\(humidity)%"
        
        let visibility = Int(weather.consolidatedWeather.first!.visibility)
        visibilityLabel.text = "\(visibility) km"

    }
}
