//
//  WeatherViewController.swift
//  BeedersWeather
//
//  Created by Willian Calazans on 28/04/19.
//  Copyright © 2019 Willian Calazans. All rights reserved.
//

import UIKit
import JGProgressHUD

class WeatherViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var localLabel: UILabel!
    @IBOutlet weak var weatherStatus: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    var weather: WeatherModel!
    
    let hud = JGProgressHUD(style: .dark)

    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hud.show(in: view)
        configCollectionView()
        loadData()
    }
    
    let refreshControl = UIRefreshControl()
    
    func configCollectionView(){
        
        weatherCollectionView.delegate = self
        weatherCollectionView.dataSource = self
        
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        weatherCollectionView.refreshControl = refreshControl
        
        weatherCollectionView.register(UINib(nibName: "WeekCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "weekCell")
        
        weatherCollectionView.register(UINib(nibName: "MoreDetailsCollectionViewCell", bundle: nil), forSupplementaryViewOfKind:UICollectionView.elementKindSectionFooter, withReuseIdentifier: "moreDetailsFooterCell")
    }
    
    @objc func refreshData(){
        loadData()
    }
    
    func loadData(){
        Routes.routeWeather { (error, weather) in
            self.weather = weather
            
            if error != nil {
                self.hud.dismiss()
                self.showAlertError()
            }
            
            if(weather.consolidatedWeather.count > 0){
                self.weatherCollectionView.reloadData()
                self.configToday()
                
                self.hud.dismiss(afterDelay: 0.0)
                self.mainView.isHidden = false
                self.weatherCollectionView.isHidden = false
                
                let locale = NSTimeZone.init(name: weather.timezone)
                NSTimeZone.default = locale! as TimeZone
            }
            
            self.gradientSelect()
            self.refreshControl.endRefreshing()            
        }
    }
    
    func showAlertError(){
        let alert = UIAlertController(title: "Error", message: "Please check your network connection and try again", preferredStyle: .alert)
        
        let refresh = UIAlertAction(title: "Refresh", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.loadData()
            self.hud.show(in: self.view)
        }
        
        alert.addAction(refresh)
        
        self.present(alert, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "moreDetailsFooterCell", for: indexPath) as? MoreDetailsCollectionViewCell
        
        if (weather != nil) {
            footerCell!.initContent(weather: weather)
            return footerCell!
        }
        
        return footerCell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: 310)
    }
    
    func configToday(){
        
        localLabel.text = weather.title
        weatherStatus.text = weather.consolidatedWeather.first?.weatherStateName
        temperatureLabel.text = "\(Int(weather.consolidatedWeather.first!.theTemp))º"
        dateTimeLabel.text = weather.consolidatedWeather.first?.applicableDate.stringToDate().nameOfTheDay()
        maxTempLabel.text = "\(Int(weather.consolidatedWeather.first!.maxTemp))º"
        minTempLabel.text = "\(Int(weather.consolidatedWeather.first!.minTemp))º"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(weather != nil){
            return weather.consolidatedWeather.count - 1
        }
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 60)

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weekCell", for: indexPath) as? WeekCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.initContent(consWeather: weather.consolidatedWeather[indexPath.row + 1])
        
        return cell
    }
    
    func gradientSelect(){
        switch weather.consolidatedWeather.first?.weatherStateAbbr {
            case "sn", "h", "sl":
                let mainColor = UIColor(red: 89.0/255.0, green: 183.0/255.0, blue: 218.0/255.0, alpha: 1.0)
                let secondColor = UIColor(red: 125.0/255.0, green: 201.0/255.0, blue: 229.0/255.0, alpha: 1.0)
                mainView.setGradientBackground(main: mainColor, second: secondColor)
                break
            case "t":
                let mainColor = UIColor(red: 50.0/255.0, green: 47/255.0, blue: 80/255.0, alpha: 1.0)
                let secondColor = UIColor(red: 54.0/255.0, green: 72.0/255.0, blue: 108.0/255.0, alpha: 1.0)
                mainView.setGradientBackground(main: mainColor, second: secondColor)
                break
            case "hr", "lr", "s", "hc":
                let mainColor = UIColor(red: 56.0/255.0, green: 80.0/255.0, blue: 118.0/255.0, alpha: 1.0)
                let secondColor = UIColor(red: 90.0/255.0, green: 97.0/255.0, blue: 118.0/255.0, alpha: 1.0)
                mainView.setGradientBackground(main: mainColor, second: secondColor)
                break
            case "lc":
                let mainColor = UIColor(red: 2.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0)
                let secondColor = UIColor(red: 80.0/255.0, green: 163.0/255.0, blue: 239.0/255.0, alpha: 1.0)
                mainView.setGradientBackground(main: mainColor, second: secondColor)
                break
            case "c":
                let mainColor = UIColor(red: 26.0/255.0, green: 101.0/255.0, blue: 186.0/255.0, alpha: 1.0)
                let secondColor = UIColor(red: 80.0/255.0, green: 163.0/255.0, blue: 239.0/255.0, alpha: 1.0)
                mainView.setGradientBackground(main: mainColor, second: secondColor)
                break
            default:
                break
        }
    }
}
